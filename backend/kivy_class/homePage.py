from kivy.uix.screenmanager import Screen


class HomePage(Screen):
    """ Home page"""
    def goto_contract_page(self):
        """ Change page to contracts """
        self.manager.transition.direction = "left"
        self.manager.current = "contract_page"

    def goto_sheep_page(self):
        """ Change page to sheep"""
        self.manager.transition.direction = "left"
        self.manager.current = "sheep_page"