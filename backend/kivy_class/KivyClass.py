from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.app import App
from kivy.clock import Clock

from backend.kivy_class.homePage import HomePage
from backend.kivy_class.contractPage import ContractPage
from backend.kivy_class.contractAddPage import ContractAddPage
from backend.kivy_class.sheepPage import SheepPage
from backend.kivy_class.sheepDetailPage import SheepDetailPage


class RootWidget(ScreenManager):
    """ Kivy screen manager """
    pass


class DummyScreen(Screen):
    def on_enter(self):
        Clock.schedule_once(self.switch_screen)

    def switch_screen(self, dt):
        self.manager.current = "sheep_page"

class MainApp(App):
    """ Main Kivy application """
    def build(self):
        return RootWidget()