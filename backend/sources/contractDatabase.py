from backend.sources.database import Database

class ContractDataBase(Database):
    def __init__(self):
        super().__init__()
        self.create()

    # Create contract database
    def create(self):
        self.connect()
        # Create a users table if doesn't exist
        self.cursor.execute(
            "CREATE TABLE IF NOT EXISTS contracts (id SERIAL NOT NULL ,title TEXT NOT NULL,location TEXT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, category INT NOT NULL, comment TEXT)")
        self.close()

    # Add new contract
    def add_contract(self, name, location, start_date, end_date, category, comment):
        self.connect()
        # Add new contract
        self.cursor.execute("INSERT INTO contracts VALUES (DEFAULT, %s, %s, %s, %s, %s, %s)", (name, location, start_date, end_date, category, comment))
        self.close()
        return True

    def find_all(self):
        """ Returns all elements from contracts table """
        self.connect()
        query = "SELECT * FROM contracts"
        self.cursor.execute(query)
        contracts = self.cursor.fetchall()
        self.close()
        return list(contracts)

    def find_by_id(self, id):
        """ Find contract by name """
        self.connect()
        query = "SELECT * FROM contracts WHERE id=%s" % id
        self.cursor.execute(query)
        contract = self.cursor.fetchall()[0]
        self.close()
        return contract

    def update_by_name(self,
                       id,
                       name,
                       location,
                       start_date,
                       end_date,
                       category,
                       comment):
        self.connect()
        self.cursor.execute("UPDATE contracts SET title=%s, location=%s WHERE id=%s",
                            (name, location, id))
        self.close()

    def delete(self, id):
        self.connect()
        self.cursor.execute("DELETE FROM contracts WHERE id=%s", (id,))
        self.close()

