import numpy as np


class Test:
    """ Test class"""
    # Fails is a table : [test id, expected, ret, msg]
    nok = 0
    total = 0
    fails = np.empty((0, 4), str)

    def log(self, test_name):
        """ Log tests """
        with open("log.txt", "a") as log:
            log.write(f"{test_name} tests : {self.nok} / {self.total} failed\n")
            log.write("===========================================\n")
            for fail in self.fails:
                log.write(f"[TEST_{fail[0]}]\texpected : {fail[1]} \
                          \n\t\t\tret      : {fail[2]} \
                          \n\t\t\tvalue    : {fail[3]}\n\n")

    def append(self, test_id, expected, ret, user_input):
        """ Append error to list """
        self.fails = np.append(self.fails, np.array([[test_id, expected, ret, user_input]]), axis=0)

    def handle_result(self, ret, expected, user_input):
        self.total += 1
        if ret != expected:
            self.nok += 1
            self.append(str(self.total), expected, ret, user_input)
