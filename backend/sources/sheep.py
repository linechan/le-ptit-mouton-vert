class Sheep:
    def __init__(self, data):
        self.id = data[0]
        self.name = data[1]
        self.race = data[2]
        self.birthdate = data[3]
        self.color = data[4]
        self.category = data[5]
        self.gender = data[6]
        self.reproduction = data[7]
        self.origin = data[8]
        self.dad = data[9]
        self.mom = data[10]
        self.sell = data[11]
        self.size = data[12]
        self.comment = data[13]

    def get_birth_year(self):
        return self.birthdate.strftime("%Y")

    def get_birth_month(self):
        return self.birthdate.strftime("%m")

    def get_birth_day(self):
        return self.birthdate.strftime("%d")