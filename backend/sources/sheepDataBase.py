from backend.sources.database import Database


class SheepDatabase(Database):
    table = "sheeps"

    def __init__(self, test_mode=False):
        super().__init__()
        if test_mode:
            self.table += "_test"

        self.create()

    def create(self):
        """ Create a sheep database"""
        self.connect()
        # Create a sheep table if doesn't exist

        query = "CREATE TABLE IF NOT EXISTS %s (id INT NOT NULL ,surname TEXT NOT NULL, race TEXT NOT NULL, \
                                                birth DATE NOT NULL, color TEXT NOT NULL, category INT NOT NULL,\
                                                gender CHAR NOT NULL, reproduction BOOLEAN NOT NULL, origin TEXT NOT NULL, \
                                                dad INT NOT NULL, mom INT NOT NULL, sell BOOLEAN, \
                                                size FLOAT, comment TEXT)" % self.table
        self.cursor.execute(query)
        self.close()

    def drop(self):
        """ Drop sheep table """
        self.connect()
        query = "DROP TABLE IF EXISTS %s" % self.table
        self.cursor.execute(query)
        self.close()

    def remove(self):
        """ Clear data from table"""
        self.connect()
        query = "DELETE FROM %s" % self.table
        self.cursor.execute(query)
        self.close()

    def add_sheep(self, sheep_id, surname, race, birth,
                  color, category, gender, reproduction,
                  origin, dad, mom, sell, size, comment):
        """ Add a new sheep to databse """
        # Check if login is available
        sheep = self.find_by_id(sheep_id)

        if sheep:
            return False

        self.connect()
        # Add new sheep
        query = "INSERT INTO %s VALUES ('%s', '%s', '%s', '%s', '%s',\
                                        '%s', '%s', '%s', '%s', '%s',\
                                        '%s', '%s', '%s', '%s')" % (
        self.table, sheep_id, surname, race, birth, color, category, gender, reproduction, origin, dad, mom, sell, size,
        comment)
        self.cursor.execute(query)
        self.close()
        return True

    def update_by_id(self, sheep_id, new_sheep_id, new_name, new_race,
                     new_birth, new_color, new_category, new_gender,
                     new_reproduction, new_origin, new_dad, new_mom,
                     new_sell, new_size, new_comment):
        """ Update sheep by id """
        self.connect()

        query = "UPDATE %s SET id = '%s',           \
                               surname = '%s',      \
                               race = '%s',         \
                               birth = '%s',        \
                               color = '%s',        \
                               category = '%s',     \
                               gender = '%s',       \
                               reproduction = '%s', \
                               origin = '%s',       \
                               dad = '%s',          \
                               mom = '%s',          \
                               sell = '%s',         \
                               size = '%s',         \
                               comment = '%s'       \
                               WHERE id = '%s'" % (self.table, new_sheep_id, new_name, new_race,
                                                   new_birth, new_color, new_category, new_gender,
                                                   new_reproduction, new_origin, new_dad, new_mom,
                                                   new_sell, new_size, new_comment, sheep_id)
        self.cursor.execute(query)
        self.close()

    def find_by_id(self, id):
        """ Find a sheep by id """
        self.connect()
        query = "SELECT * FROM %s WHERE id='%s'" % (self.table, id)
        self.cursor.execute(query)
        sheep = self.cursor.fetchall()
        self.close()
        return sheep

    def find_all(self):
        """ Returns all elements from sheep table """
        self.connect()
        query = "SELECT * FROM %s" % self.table
        self.cursor.execute(query)
        sheeps = self.cursor.fetchall()
        self.close()
        return list(sheeps)

    def find_by_name(self, name):
        """ Find a sheep by name """
        self.connect()
        query = "SELECT * FROM %s WHERE surname='%s'" % (self.table, name)
        self.cursor.execute(query)
        sheep = self.cursor.fetchall()
        self.close()
        return sheep
