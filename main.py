
from backend.sources.sheepDatabase import SheepDatabase
from backend.sources.contractDatabase import ContractDataBase

from kivy.lang import Builder
import glob
from backend.kivy_class.KivyClass import MainApp

if __name__ == '__main__':
    kivy_files = glob.glob("./frontend/widget/*.kv")

    for file in kivy_files:
        Builder.load_file(file)
    MainApp().run()
