import json
import psycopg2

class Database:
    def __init__(self):
        """ Read info to access database """
        with open("./ressources/credentials.json") as file:
            credential = json.load(file)

        self.host = credential["host"]
        self.database = credential["database"]
        self.user = credential["user"]
        self.password = credential["password"]
        self.conn = None
        self.cursor = None

    def connect(self):
        """ Create a connector to the database"""
        self.conn = psycopg2.connect(host=self.host,
                                     database=self.database,
                                     user=self.user,
                                     password=self.password)

        self.cursor = self.conn.cursor()

    def close(self):
        """ Close contract databse """
        # Commit changes
        self.conn.commit()
        # Close connexion
        self.cursor.close()
        self.conn.close()

    def __del__(self):
        if self.conn:
            self.cursor.close()
            self.conn.close()
