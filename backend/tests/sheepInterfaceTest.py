from backend.kivy_class.sheepDetailPage import SheepDetailPage
from backend.sources.sheep import Sheep
from backend.tests.test import Test
import numpy as np


class SheepInterfaceTest(Test):
    """ Test class for Sheep Page form """

    def __init__(self):
        super().__init__()

    def run(self):
        """ Run tests"""
        screen = SheepDetailPage()
        screen.init_test()

        # ADD tests
        self.test_add(screen)
        super().log("Add Sheep")
        screen.db.remove()
        # DISPLAY tests
        self.test_display(screen)
        super().log("Display Sheep")
        screen.db.remove()
        # UPDATE tests
        self.test_update(screen)
        super().log("Update Sheep")
        # Drop test table from database
        screen.db.drop()

    def init_valid_sheep(self, screen):
        screen.ids.name.text = "test"
        screen.ids.sheep_id.text = "1"
        screen.ids.race.text = "ouessant"
        screen.ids.birth_year.text = "2020"
        screen.ids.birth_month.text = ""
        screen.ids.birth_day.text = ""
        screen.ids.color.text = "marron"
        screen.ids.category.text = ""
        screen.ids.gender.text = "M"
        screen.ids.reproduction.text = "Oui"
        screen.ids.origin.text = "FR1"
        screen.ids.dad.text = "2"
        screen.ids.mom.text = "3"
        screen.ids.sell.text = "Oui"
        screen.ids.size.text = "40.0"
        screen.ids.comment.text = "comment"

    def reset(self, screen):
        """ Reset attribute """
        self.nok = 0
        self.total = 0
        self.fails = np.empty((0, 4), str)
        screen.clear_inputs()
        screen.ids.msg.text = ""
        screen.sheep = None

    def test_display(self, screen):
        """ Sheep detail page"""

        # Call method
        self.reset(screen)
        self.init_valid_sheep(screen)
        screen.save()
        screen.display("test")

        # NAME
        self.handle_result(screen.ids.name.text, "Test", "screen.ids.name.text")
        # ID
        self.handle_result(screen.ids.sheep_id.text, "1", "screen.ids.sheep_id.text")
        # RACE
        self.handle_result(screen.ids.race.text, "ouessant", "screen.ids.race.text")
        # BIRTH YEAR
        self.handle_result(screen.ids.birth_year.text, "2020", "screen.ids.birth_year.text")
        # BIRTH MONTH
        self.handle_result(screen.ids.birth_month.text, "01", "screen.ids.birth_month.text")
        # BIRTH DAY
        self.handle_result(screen.ids.birth_day.text, "01", "screen.ids.birth_day.text")
        # BIRTH COLOR
        self.handle_result(screen.ids.color.text, "marron", "screen.ids.color.text")
        # CATEGORY
        self.handle_result(screen.ids.category.text, "0", "screen.ids.category.text")
        # GENDER
        self.handle_result(screen.ids.gender.text, "m", "screen.ids.gender.text")
        # REPRODUCTION
        self.handle_result(screen.ids.reproduction.text, "Oui", "screen.ids.reproduction.text")
        # ORIGIN
        self.handle_result(screen.ids.origin.text, "FR1", "screen.ids.origin.text")
        # DAD
        self.handle_result(screen.ids.dad.text, "2", "screen.ids.dad.text")
        # MOM
        self.handle_result(screen.ids.mom.text, "3", "screen.ids.mom.text")
        # SELL
        self.handle_result(screen.ids.sell.text, "Oui", "screen.ids.sell.text")
        # SIZE
        self.handle_result(screen.ids.size.text, "40.0", "screen.ids.size.text")
        # COMMENT
        self.handle_result(screen.ids.comment.text, "comment", "screen.ids.comment.text")

    def test_add(self, screen):
        """ Adding sheep to databse """
        # VALID INPUTS
        self.init_valid_sheep(screen)
        self.handle_result(screen.save(), True, "Sheep not added")

        # NAME : already taken
        self.init_valid_sheep(screen)
        self.handle_result(screen.save(), "NAME : already taken", screen.ids.name.text)
        # NAME : empty
        screen.ids.name.text = ""
        self.handle_result(screen.save(), "NAME : empty", screen.ids.name.text)
        # SHEEP ID : already taken
        screen.ids.name.text = "SHEEP_NAME"
        self.handle_result(screen.save(), "SHEEP ID : already taken", screen.ids.sheep_id.text)
        # SHEEP ID : empty
        screen.ids.sheep_id.text = ""
        self.handle_result(screen.save(), "SHEEP ID : empty", screen.ids.sheep_id.text)
        # SHEP ID : not an integer
        screen.ids.sheep_id.text = "A"
        self.handle_result(screen.save(), "SHEP ID : not an integer", screen.ids.sheep_id.text)

        # RACE : invalid
        screen.ids.sheep_id.text = "2"
        screen.ids.race.text = "invalid_race"
        self.handle_result(screen.save(), "RACE : invalid", screen.ids.race.text)

        # BIRTH : invalid day
        screen.ids.race.text = "ouessant"
        screen.ids.birth_day.text = "42"
        self.handle_result(screen.save(), "BIRTH : invalid", screen.ids.birth_day.text)

        # BIRTH : invalid month
        screen.ids.birth_day.text = "1"
        screen.ids.birth_month.text = "42"
        self.handle_result(screen.save(), "BIRTH : invalid", screen.ids.birth_month.text)

        # BIRTH : invalid year
        screen.ids.birth_month.text = "1"
        screen.ids.birth_year.text = ""
        self.handle_result(screen.save(), "BIRTH : invalid", screen.ids.birth_year.text)

        # COLOR : invalid
        screen.ids.birth_year.text = "2020"
        screen.ids.color.text = "vert"
        self.handle_result(screen.save(), "COLOR : invalid", screen.ids.color.text)

        # CATEGORY : not an integer
        screen.ids.color.text = "marron"
        screen.ids.category.text = "vert"
        self.handle_result(screen.save(), "CATEGORY : not an integer", screen.ids.color.text)

        # GENDER : empty
        screen.ids.category.text = ""
        screen.ids.gender.text = ""
        self.handle_result(screen.save(), "GENDER : empty", screen.ids.gender.text)
        # GENDER : invalid
        screen.ids.gender.text = "G"
        self.handle_result(screen.save(), "GENDER : invalid", screen.ids.gender.text)

        # ORIGIN : empty
        screen.ids.gender.text = "M"
        screen.ids.origin.text = ""
        self.handle_result(screen.save(), "ORIGIN : empty", screen.ids.origin.text)

        # SIZE : not an integer
        screen.ids.origin.text = "FR"
        screen.ids.size.text = "A"
        self.handle_result(screen.save(), "SIZE : not an integer", screen.ids.origin.text)

    def test_update(self, screen):
        """ Update sheep info test """
        # Test setup
        self.reset(screen)
        self.init_valid_sheep(screen)
        screen.save()
        screen.sheep = Sheep(screen.db.find_by_name("test")[0])

        # Change
        screen.ids.name.text = "new_test"
        screen.ids.sheep_id.text = "2"
        screen.ids.race.text = "ardennais"
        screen.ids.birth_year.text = "2019"
        screen.ids.birth_month.text = "12"
        screen.ids.birth_day.text = "31"
        screen.ids.color.text = "noir"
        screen.ids.category.text = "1"
        screen.ids.gender.text = "F"
        screen.ids.reproduction.text = ""
        screen.ids.origin.text = "FR2"
        screen.ids.dad.text = "3"
        screen.ids.mom.text = "4"
        screen.ids.sell.text = ""
        screen.ids.size.text = ""
        screen.ids.comment.text = "new_comment"

        screen.update()

        sheep = screen.sheep
        # Valid current sheep
        self.handle_result(sheep != None, True, "invalid current sheep")
        # SHEEP_ID
        self.handle_result(sheep.id, 2, "sheep_id")
        # NAME
        self.handle_result(sheep.name, "new_test", "name")
        # RACE
        self.handle_result(sheep.race, "ardennais", "race")
        # BIRTH : year
        self.handle_result(sheep.get_birth_year(), "2019", "birth year")
        # BIRTH : month
        self.handle_result(sheep.get_birth_month(), "12", "birth month")
        # BIRTH : day
        self.handle_result(sheep.get_birth_day(), "31", "birth day")
        # COLOR
        self.handle_result(sheep.color, "noir", "color")
        # CATEGORY
        self.handle_result( str(sheep.category), "1", "category")
        # GENDER
        self.handle_result(sheep.gender, "f", "gender")
        # REPRODUCTION
        self.handle_result("Oui" if sheep.reproduction is True else "Non", "Non", "gender")
        # ORIGIN
        self.handle_result(sheep.origin, "FR2", "origin")
        # DAD
        self.handle_result(str(sheep.dad), "3", "father")
        # MOM
        self.handle_result(str(sheep.mom), "4", "mother")
        # SELL
        self.handle_result("Oui" if sheep.sell is True else "Non", "Non", "sell")
        # SIZE
        self.handle_result(str(sheep.size), "0.0", "size")
        # COMMENT
        self.handle_result(sheep.comment, "new_comment", "comment")

        # Save sheep with id #1 name : "test"
        self.reset(screen)
        self.init_valid_sheep(screen)
        screen.save()

        screen.sheep = Sheep(screen.db.find_by_id(2)[0])

        # SHEEP ID : already taken
        screen.ids.sheep_id.text = "1"
        self.handle_result(screen.update(), "SHEEP ID : already taken", screen.ids.sheep_id.text)

        # NAME : already taken
        screen.ids.sheep_id.text = str(screen.sheep.id)
        screen.ids.name.text = "test"
        self.handle_result(screen.update(), "NAME : already taken", screen.ids.name.text)