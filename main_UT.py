import os
from backend.sources.sheepDatabase import SheepDatabase
from backend.sources.contractDatabase import ContractDataBase
from backend.tests.sheepInterfaceTest import SheepInterfaceTest
from backend.tests.sheepTest import SheepTest

from kivy.lang import Builder
import glob
from backend.kivy_class.KivyClass import MainApp

if __name__ == '__main__':
    kivy_files = glob.glob("./frontend/widget/*.kv")

    for file in kivy_files:
        Builder.load_file(file)

    if os.path.isfile("log.txt"):
        os.remove("log.txt")
    with open("./log.txt", "w"): pass
    SheepInterfaceTest().run()
    SheepTest().run()
