from kivy.uix.screenmanager import Screen


from backend.sources.contractDatabase import ContractDataBase
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from kivy.core.window import Window
from kivy.uix.gridlayout import GridLayout

from datetime import datetime


class ContractPage(Screen):
    """ Contract Page class"""
    def on_pre_enter(self, *args):
        """ Create list of contracts """
        layout = GridLayout(cols=1, size_hint_y=None)
        # Make sure the height is such that there is something to scroll.
        layout.bind(minimum_height=layout.setter('height'))
        db = ContractDataBase()
        contracts = db.find_all()
        contracts.sort(key=lambda x: x[1])
        for contract in contracts:
            btn = Button(id=str(contract[0]), text=str(contract[1]), size_hint_y=None, height=40)
            btn.bind(on_press=self.change_label)
            layout.add_widget(btn)
        root = ScrollView(size_hint=(1, None), size=(Window.width, 500))
        root.add_widget(layout)
        self.ids.contract_list.add_widget(root)

    def on_leave(self, *args):
        """ Clear page """
        self.ids.contract_list.clear_widgets()
        self.ids.name.text = ""
        self.ids.name.property = "0"

    def change_label(self, instance):
        """ Update label with selected contract """
        self.ids.name.text = instance.text
        self.ids.name.property = instance.id

    def goto_contract_detail(self, name):
        """ Change page to contract details """
        if name == "":
            self.ids.name.hint_text = "Selectionnez un contrat"
            return

        page = self.manager.get_screen('contract_add_page')
        db = ContractDataBase()
        contract = db.find_by_id(self.ids.name.property)
        page.ids.name.text = contract[1]
        page.ids.location.text = contract[2]
        page.ids.start_day.text = contract[3].strftime("%d")
        page.ids.start_month.text = contract[3].strftime("%m")
        page.ids.start_year.text = contract[3].strftime("%Y")
        page.ids.end_day.text = contract[4].strftime("%d")
        page.ids.end_month.text = contract[4].strftime("%m")
        page.ids.end_year.text = contract[4].strftime("%Y")
        page.ids.category.text = str(contract[5])
        page.ids.comment.text = contract[6]
        if datetime.now().date() > contract[4]:
            page.ids.msg.text = "Contrat termine"
        elif datetime.now().date() < contract[3]:
            page.ids.msg.text = "Contrat a venir"
        else:
            page.ids.msg.text = "Contrat en cours"
        self.manager.current = "contract_add_page"
        self.manager.transition.direction = "left"
        self.manager.get_screen('contract_add_page').ids.form.property = self.ids.name.property

    def goto_add_contract_page(self):
        """ Change to contract page """
        self.manager.get_screen('contract_add_page').ids.form.property = "0"
        self.manager.current = "contract_add_page"
        self.manager.transition.direction = "left"

    def goto_home_page(self):
        """ Change to home page"""
        self.manager.current = "home_page"
        self.manager.transition.direction = "right"
