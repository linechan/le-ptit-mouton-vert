from kivy.uix.screenmanager import Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.button import Button
from kivy.core.window import Window
from backend.sources.sheepDatabase import SheepDatabase


class SheepPage(Screen):
    """ Sheep Page class """

    def on_pre_enter(self, *args):
        """ Create list of contracts """
        layout = GridLayout(cols=1, size_hint_y=None)
        # Make sure the height is such that there is something to scroll
        layout.bind(minimum_height=layout.setter('height'))

        db = SheepDatabase()
        cattle = db.find_all()
        cattle.sort(key=lambda x: x[1])
        for sheep in cattle:
            btn = Button(text=str(sheep[1]), size_hint_y=None, height=40)
            btn.bind(on_press=self.change_label)
            layout.add_widget(btn)
        root = ScrollView(size_hint=(1, None), size=(Window.width, 400))
        root.add_widget(layout)
        self.ids.sheep_list.add_widget(root)

    def goto_sheep_add_page(self):
        self.manager.current = "sheep_detail_page"
        self.manager.transition.direction = "left"

    def on_leave(self, *args):
        """ Clear page """
        self.ids.sheep_list.clear_widgets()

    def change_label(self, instance):
        """ Update label with selected sheep """
        self.ids.name.text = instance.text

    def goto_sheep_detail_page(self):
        if self.ids.name.text == "":
            self.ids.name.hint_text = "Selectionnez un mouton dans la liste"
            return
        self.manager.get_screen('sheep_detail_page').display(self.ids.name.text.lower())
        self.goto_sheep_add_page()

