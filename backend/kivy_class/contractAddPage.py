from kivy.uix.screenmanager import Screen
from backend.sources.contractDatabase import ContractDataBase
from datetime import datetime

class ContractAddPage(Screen):
    def goto_contract_page(self):
        """ Change page to contract """
        self.manager.current = "contract_page"
        self.manager.transition.direction= "right"

    def save(self):
        """ Add new contract to database """

        # Check empty name
        if self.ids.name.text == "":
            self.ids.msg.text = "Remplir le nom"
            return

        # Check empty location
        if self.ids.location.text == "":
            self.ids.msg.text = "Remplir le lieu"
            return

        if self.ids.category.text == "":
            self.ids.category.text = "0"

        # Start date
        try:
            start_date = datetime(int(self.ids.start_year.text),
                                  int(self.ids.start_month.text),
                                  int(self.ids.start_day.text))
            self.ids.msg.text = ""
        except ValueError:
            self.ids.msg.text = "Date debut de contrat invalide"
            return

        # End date
        try:
            end_date = datetime(int(self.ids.end_year.text),
                                  int(self.ids.end_month.text),
                                  int(self.ids.end_day.text))
            self.ids.msg.text = ""
        except ValueError:
            self.ids.msg.text = "Date fin de contrat invalide"
            return

        # Compare date
        if end_date < start_date:
            self.ids.msg.text = "La date de fin de contrat doit preceder la date de debut"
            return

        db = ContractDataBase()

        if self.ids.form.property == "0":
            db.add_contract(self.ids.name.text,
                            self.ids.location.text,
                            start_date,
                            end_date,
                            self.ids.category.text,
                            self.ids.comment.text)
            self.ids.msg.text = "Nouveau contrat ajoute"
            self.clear_inputs()
        else :
            db.update_by_name(self.ids.form.property,
                              self.ids.name.text,
                            self.ids.location.text,
                            start_date,
                            end_date,
                            self.ids.category.text,
                            self.ids.comment.text)
            self.ids.msg.text = "Contrat modifie"

    def on_leave(self, *args):
        self.clear_inputs()
        self.ids.msg.text = ""

    def clear_inputs(self):
        """ Clear page """
        self.ids.name.text = ""
        self.ids.location.text = ""
        self.ids.start_day.text = ""
        self.ids.start_month.text = ""
        self.ids.start_year.text = ""
        self.ids.end_day.text = ""
        self.ids.end_month.text = ""
        self.ids.end_year.text = ""
        self.ids.category.text = ""
        self.ids.comment.text = ""
        self.ids.form.property = "0"

    def delete(self):
        """ Delete """
        db = ContractDataBase()
        db.delete(self.ids.form.property)
        self.ids.msg.text = "Contrat supprime"
        self.clear_inputs()