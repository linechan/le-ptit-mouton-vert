from kivy.uix.screenmanager import Screen
from datetime import datetime
from backend.sources.sheepDatabase import SheepDatabase
from backend.sources.sheep import Sheep


class SheepDetailPage(Screen):
    db = SheepDatabase()
    sheep = None

    def init_test(self):
        """ Init testing database """
        self.db = SheepDatabase(True)

    def update(self):
        self.ids.msg.text = ""
        # Check if new id
        if self.ids.sheep_id.text != str(self.sheep.id):
            ret = self.check_sheep_id()
            if ret is not True:
                self.ids.msg.text += " : non mis a jour"
                return "SHEEP ID : already taken"
        self.ids.name.text = self.ids.name.text.lower()
        if self.ids.name.text != self.sheep.name:
            ret = self.check_name()
            if ret is not True:
                self.ids.msg.text += " : non mis a jour"
                return ret

        ret = self.check()
        if isinstance(self.check(), str):
            self.ids.msg.text += " : non mis a jour"
            return ret

        self.db.update_by_id(self.sheep.id,
                             self.ids.sheep_id.text,
                             self.ids.name.text,
                             self.ids.race.text,
                             ret[0],
                             self.ids.color.text,
                             self.ids.category.text,
                             self.ids.gender.text,
                             ret[1],
                             self.ids.origin.text,
                             self.ids.dad.text,
                             self.ids.mom.text,
                             ret[2],
                             self.ids.size.text,
                             self.ids.comment.text)
        # Update current sheep
        self.sheep = Sheep(self.db.find_by_name(self.ids.name.text)[0])

    def update_current_sheep(self):
        pass


    def display(self, name):
        """ Display sheep info on screen """
        self.sheep = Sheep(self.db.find_by_name(name)[0])
        self.ids.sheep_id.text = str(self.sheep.id)
        self.ids.name.text = self.sheep.name.capitalize()
        self.ids.race.text = self.sheep.race
        self.ids.birth_year.text = self.sheep.get_birth_year()
        self.ids.birth_month.text = self.sheep.get_birth_month()
        self.ids.birth_day.text = self.sheep.get_birth_day()
        self.ids.color.text = self.sheep.color
        self.ids.category.text = str(self.sheep.category)
        self.ids.gender.text = self.sheep.gender
        self.ids.reproduction.text = "Oui" if self.sheep.reproduction is True else "Non"
        self.ids.origin.text = self.sheep.origin
        self.ids.dad.text = str(self.sheep.dad)
        self.ids.mom.text = str(self.sheep.mom)
        self.ids.sell.text = "Oui" if self.sheep.sell is True else "Non"
        self.ids.size.text = str(self.sheep.size)
        self.ids.comment.text = self.sheep.comment

    def save(self):
        if self.sheep is not None:
            self.update()
            return
        """" Add sheep to database """
        ret = self.check_all()
        if isinstance(ret, str):
            return ret
        else:
            self.ids.msg.text = "Nouveau mouton ajoute"
            self.db.add_sheep(self.ids.sheep_id.text, self.ids.name.text, self.ids.race.text,
                              ret[0], self.ids.color.text, self.ids.category.text, self.ids.gender.text,
                              ret[1], self.ids.origin.text, self.ids.dad.text,
                              self.ids.mom.text, ret[2], self.ids.size.text, self.ids.comment.text)
            self.clear_inputs()
        return True

    def check_all(self):
        # Check name
        ret = self.check_name()
        if ret is not True:
            return ret

        # Check id
        ret = self.check_sheep_id()
        if ret is not True:
            return ret

        return self.check()

    def check(self):
        self.ids.msg.text = ""
        # Check race
        ret = self.check_race()
        if ret is not True:
            return ret

        # Check birthday
        birthday = self.check_birthday()
        if isinstance(birthday, str):
            return birthday

        # Check color
        ret = self.check_color()
        if ret is not True:
            return ret

        # Check category
        ret = self.check_category()
        if ret is not True:
            return ret

        # Check gender
        ret = self.check_gender()
        if ret is not True:
            return ret

        # Check origin
        ret = self.check_origin()
        if ret is not True:
            return ret

        # Check dad
        ret = self.check_dad()
        if ret is not True:
            return ret

        # Check mom
        ret = self.check_mom()
        if ret is not True:
            return ret

        is_to_sell = self.check_sell()

        # Check if the sheep can be reproduced
        is_to_reproduce = self.check_reproduction()

        # Check size
        ret = self.check_size()
        if ret is not True:
            return ret

        return birthday, is_to_reproduce, is_to_sell

    def clear_inputs(self):
        self.ids.name.text = ""
        self.ids.sheep_id.text = ""
        self.ids.race.text = ""
        self.ids.birth_year.text = ""
        self.ids.birth_month.text = ""
        self.ids.birth_day.text = ""
        self.ids.color.text = ""
        self.ids.category.text = ""
        self.ids.gender.text = ""
        self.ids.reproduction.text = ""
        self.ids.origin.text = ""
        self.ids.dad.text = ""
        self.ids.mom.text = ""
        self.ids.sell.text = ""
        self.ids.size.text = ""
        self.ids.comment.text = ""

    def on_leave(self):
        self.clear_inputs()
        self.ids.msg.text = ""
        self.sheep = None

    def goto_sheep_page(self):
        self.manager.current = "sheep_page"
        self.manager.transition.direction = "right"

    def check_name(self):
        """ Check sheep name"""
        # Empty name
        if self.ids.name.text == "":
            self.ids.msg.text = "Remplir le nom du mouton"
            return "NAME : empty"

        # Search in database if the sheep is referenced already
        sheep = self.db.find_by_name(self.ids.name.text.lower())
        if sheep:
            self.ids.msg.text = "Ce nom est deja attribue a un autre mouton"
            return "NAME : already taken"
        self.ids.name.text = self.ids.name.text.lower()
        return True

    def check_sheep_id(self):
        if self.ids.sheep_id.text == "":
            self.ids.msg.text = "Remplir le numero de bague"
            return "SHEEP ID : empty"
        try:
            id = int(self.ids.sheep_id.text)
        except:
            self.ids.msg.text = "Numero de bague invalide"
            return "SHEP ID : not an integer"
        else:
            if self.db.find_by_id(id):
                self.ids.msg.text = "Le numero de bague est deja attribue"
                return "SHEEP ID : already taken"
        return True

    def check_color(self):
        lower_color = self.ids.color.text.lower()
        if lower_color != "noir" \
                and lower_color != "marron" \
                and lower_color != "blanc":
            self.ids.msg.text = "La couleur n'est pas valide (noir, marron, blanc)"
            return "COLOR : invalid"
        self.ids.color.text = lower_color
        return True

    def check_gender(self):
        lower_gender = self.ids.gender.text.lower()
        if lower_gender == "":
            return "GENDER : empty"

        if lower_gender != "m" and self.ids.gender.text.lower() != "f":
            self.ids.msg.text = "Le sexe n'est pas valide (M ou F)"
            return "GENDER : invalid"
        self.ids.gender.text = lower_gender
        return True

    def check_race(self):
        if self.ids.race.text.lower() != "ouessant" and self.ids.race.text.lower() != "ardennais":
            self.ids.msg.text = "La race doit etre Ouessant ou Ardennais"
            return "RACE : invalid"
        return True

    def check_origin(self):
        if self.ids.origin.text == "":
            self.ids.msg.text = "Entrez un nom d'eleveur d'origine"
            return "ORIGIN : empty"
        return True

    def check_sell(self):
        # Check if the sheep is for sell
        if self.ids.sell.text.lower() == "oui":
            return True
        else:
            return False

    def check_category(self):
        if self.ids.category.text == "":
            self.ids.category.text = "0"

        try:
            int(self.ids.category.text)
        except:
            self.ids.msg.text = "La categorie n'est pas valide (1, 2, 3)"
            return "CATEGORY : not an integer"
        else:
            return True

    def check_reproduction(self):
        if self.ids.reproduction.text.lower() == "oui":
            return True
        else:
            return False

    def check_size(self):
        if self.ids.size.text == "":
            self.ids.size.text = "0"
        try:
            float(self.ids.size.text)
        except:
            self.ids.msg.text = "Taille non valide"
            return "SIZE : not an integer"
        else:
            return True

    def check_birthday(self):
        if self.ids.birth_month.text == "":
            self.ids.birth_month.text = "1"
        if self.ids.birth_day.text == "":
            self.ids.birth_day.text = "1"
        try:
            birthday = datetime(int(self.ids.birth_year.text),
                                int(self.ids.birth_month.text),
                                int(self.ids.birth_day.text))
        except ValueError:
            self.ids.msg.text = "La date de naissance n'est pas valide"
            return str("BIRTH : invalid")
        else:
            return birthday

    def check_dad(self):
        if self.ids.dad.text == "":
            self.ids.dad.text = "0"
        return True

    def check_mom(self):
        if self.ids.mom.text == "":
            self.ids.mom.text = "0"
        return True