from backend.sources.sheep import Sheep
from backend.sources.sheepDatabase import SheepDatabase
from backend.tests.test import Test
from datetime import datetime

class SheepTest(Test):
    """ Sheep test class """
    def __init__(self):
        super().__init__()

    def run(self):
        """ Run tests"""
        db = SheepDatabase(True)
        birthdate = datetime(2020, 10, 11)
        db.add_sheep(1, "test_name", "ouessant", birthdate, "marron", 0, "m", True, "FR", 2, 3, False, "40.0", "comment")
        sheep = Sheep(db.find_by_id(1)[0])

        # ID
        self.handle_result(sheep.id, 1, sheep.id)
        # NAME
        self.handle_result(sheep.name, "test_name", sheep.name)
        # RACE
        self.handle_result(sheep.race, "ouessant", sheep.race)
        # BIRTHDATE
        birthdate = datetime(int(sheep.get_birth_year()),
                             int(sheep.get_birth_month()),
                             int(sheep.get_birth_day()))
        self.handle_result(sheep.birthdate, birthdate.date(), sheep.birthdate)
        # COLOR
        self.handle_result(sheep.color, "marron", sheep.color)
        # CATEGORY
        self.handle_result(sheep.category, 0, sheep.category)
        # GENDER
        self.handle_result(sheep.gender, "m", sheep.gender)
        # REPRODUCTION
        self.handle_result(sheep.reproduction, True, sheep.reproduction)
        # ORIGIN
        self.handle_result(sheep.origin, "FR", sheep.origin)
        # DAD
        self.handle_result(sheep.dad, 2, sheep.dad)
        # MOM
        self.handle_result(sheep.mom, 3, sheep.mom)
        # SELL
        self.handle_result(sheep.sell, False, sheep.sell)
        # SIZE
        self.handle_result(sheep.size, 40.0, sheep.size)
        # COMMENT
        self.handle_result(sheep.comment, "comment", sheep.comment)

        db.remove()
        super().log("Sheep class")
